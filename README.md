# Medical Product Rating rest Api

## Introduction
This API can create, update,read Medicalproduct Pating data.Assuming that the patient have used the given product.There is also a endpoint created for registering a patient  and getting the list of patient.

## Error Code
Error Code-404 NOT FOUND can be throw to User. If Wrong Inputs given to the API

## Assumption  Made
- That patien should be registered when he uses Medical product only then we he will be able to give the ratings for products
- Presently for given problem statement There are only 5 products and that is already loaded in the database
- When saving product rating it has to get product id and patient id
  and rating value all of this can't be empty.

# Design flow Chart
[Link for flow chart](https://adhikarisuvidh-my.sharepoint.com/:u:/g/personal/adhikarisuvidh9_adhikarisuvidh_onmicrosoft_com/EQyrlq5oMkNMiDY_NQihVkYBbz0ToV1VBzlEcqY1oaceEg?e=4%3AedNxtD&at=9)
## Technology Stack
I used the following technologieds for the application created in sts.
  - Spring boot 2.4
  - Spring MVC
  - Spring Data JPA
  - Hibernate
  - H2 In-Memory Database Embedded
  - Springfox-swagger2
  - Springfox-swagger-ui
  - Apache Maven

## Client Use Flow
1. Run the spring boot application.
2. The embedded tomcat server will start on your localhost at ***PORT:8080***
3. The client can Test or Use Thsi Specific API on Postman
4. Now You can access the data in H2 Database:<http://localhost:8080/console>
5. data base properties
    >spring.datasource.url=jdbc:h2:mem:daytoday
    >spring.datasource.username=root
    >spring.datasource.password=root
6. I have used Swaggar For Documentation and can be retrieve by this path <http://localhost:8080/swagger-ui.html>
7. I have preloaed Medical Product Data assuming there are only 5 medical products and patient, ProductRating data for testing  in data.sql file.






