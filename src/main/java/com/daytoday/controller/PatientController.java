package com.daytoday.controller;

import java.util.List ; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daytoday.models.Patient;
import com.daytoday.service.PatientService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class PatientController
{
	private PatientService patientService;

	@Autowired
	public PatientController(PatientService patientService) {
		this.patientService = patientService;
	}
	
	
	
	@ApiOperation(value = "fetching list of patients")
	@GetMapping("/patients")
	public ResponseEntity<List<Patient>> getPatients()
	{
		return new ResponseEntity<>(patientService.getPatients(),HttpStatus.OK);
	}
	@ApiOperation(value=" registering a patient",response =Patient.class)
	@PostMapping("/patient")
	public ResponseEntity<Patient> savePatient(@RequestBody Patient patient)
	{
		Patient patient1 = patientService.savepatient(patient);
		if (patient1==null) return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Patient>(patientService.savepatient(patient), HttpStatus.OK);
	}
	
}
