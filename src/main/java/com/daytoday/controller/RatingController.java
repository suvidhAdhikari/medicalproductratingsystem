package com.daytoday.controller;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daytoday.models.ProductRating;
import com.daytoday.models.ProductRatingResult;
import com.daytoday.service.ProductRatingService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
public class RatingController 
{
	private ProductRatingService productRatingService;

	@Autowired
	public RatingController(ProductRatingService productRatingService) 
	{
		this.productRatingService = productRatingService;
	}
	

	@ApiOperation(value = "gets the list of rating")
	@ApiResponses(value = {
			@ApiResponse( code = 200, message ="getting list of product  is successful"),
			@ApiResponse(code=404 , message = "when there is no list found")
	})
	@GetMapping("/ratings")
	public ResponseEntity<List<ProductRating>> getRatings()
	{
		List<ProductRating> ratings = productRatingService.getRatings();
		if (ratings==null) return new ResponseEntity<List<ProductRating>>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<List<ProductRating>>(ratings,HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "gets the averge rating, product details, count of individual rating",response = ProductRating.class)
	@ApiResponses(value = {
			@ApiResponse(code=200,message = " when product if found "),
			@ApiResponse(code = 404,message = " when the given product is not found")
	})
	@GetMapping("/ratings/product/{p_id}")
	public ResponseEntity< ProductRatingResult> getProductRating(@PathVariable("p_id")Long p_id)
	{
		 ProductRatingResult ratingOfProduct = productRatingService.getRatingOfProduct(p_id);
		 if (ratingOfProduct==null) return new ResponseEntity<>(ratingOfProduct,HttpStatus.NOT_FOUND);
		 return new ResponseEntity<ProductRatingResult>(ratingOfProduct,HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "adding a rating a product for a paticular medical product by patient ",response = ProductRating.class)
	@ApiResponses(value={ 
			@ApiResponse(code =201,message = "when the product is added successfully" ),
			@ApiResponse(code=400,message = "adding a product was unsuccessful ")
	})
	@PostMapping("/rating")
	public ResponseEntity<ProductRating> saveProductRating(@RequestBody ProductRating productRating) 
	{
		ProductRating rating = productRatingService.saveProductRating(productRating);
		if (rating!=null) return new ResponseEntity<>(rating,HttpStatus.CREATED);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	
	@ApiOperation(value = " updating the rating  ")
	@ApiResponses(value = {
			@ApiResponse(code =200,message = "when update operation is successful" ),
			@ApiResponse(code = 400,message = "when update operation is not successful")
			
	})
	@PutMapping("/rating")
	public ResponseEntity<ProductRating> updateProductRating(@RequestBody ProductRating productRating)
	{
		ProductRating rating = productRatingService.updatePrductRating(productRating);
		if(rating!=null) return new ResponseEntity<>(rating, HttpStatus.OK);
		else return new ResponseEntity<>(rating, HttpStatus.BAD_REQUEST); 	
	}
	
}
