package com.daytoday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.daytoday.models.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long>
{
	
}
