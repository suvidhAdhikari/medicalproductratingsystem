package com.daytoday.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.daytoday.models.ProRatId;
import com.daytoday.models.ProductRating;
@Repository
public interface ProductRatingRepository extends JpaRepository<ProductRating,ProRatId> {
	@Query(value ="select pr.rating from product_rating pr where product_id= :p_id", nativeQuery = true )
	List<Integer> findRatingByProductid(@Param("p_id") long p_id);
}
