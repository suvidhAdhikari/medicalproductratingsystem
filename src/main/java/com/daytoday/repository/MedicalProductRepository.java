package com.daytoday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.daytoday.models.MedicalProduct;

public interface MedicalProductRepository extends JpaRepository<MedicalProduct,Long>
{
	String findNameById(Long id);
}
