package com.daytoday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalProductRatingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicalProductRatingApplication.class, args);
	}

}
