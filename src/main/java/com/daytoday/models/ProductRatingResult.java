package com.daytoday.models;

import java.util.Map;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "this model is used for returning result of medical product rating")
public class ProductRatingResult
{
	@ApiModelProperty(notes = "medical product details")
	private MedicalProduct medicalProduct;
	
	@ApiModelProperty(notes = "average rating for a particular product")
	private Double avgRating;

	@ApiModelProperty(notes = "count of indivisual rating")
	Map<Integer, Integer> ratingCount;
	
	public ProductRatingResult()
	{
		
	}

	public MedicalProduct getMedicalProduct() {
		return medicalProduct;
	}


	public void setMedicalProduct(MedicalProduct medicalProduct) {
		this.medicalProduct = medicalProduct;
	}


	public Double getAvgRating() {
		return avgRating;
	}
	
	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Map<Integer, Integer> getRatingCount() {
		return ratingCount;
	}
	public void setRatingCount(Map<Integer, Integer> ratingCount) {
		this.ratingCount = ratingCount;
	}
}
