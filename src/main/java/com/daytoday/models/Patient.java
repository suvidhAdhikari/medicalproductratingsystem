package com.daytoday.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;

@Entity
@ApiModel(description = "this model is used to store patient details")
public class Patient implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6612779775153559900L;

	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	private Long id;
	
	private String user_name;

	
	private Patient() {
	// TODO Auto-generated constructor stub
	}
	
	//Getters and setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
}
