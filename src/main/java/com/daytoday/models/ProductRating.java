package com.daytoday.models;

import javax.persistence.CascadeType; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.swagger.annotations.ApiModel;

@Entity
@Table(name = "PRODUCT_RATING")
@IdClass(ProRatId.class)
@ApiModel(description = "this is a model to get the deatils from ui "
		+ "and this class contains composite key as primary key(product_id,patient_id)")
public class ProductRating 
{	
	private static final long serialVersionUID = 1725721737832818874L;
	

	@Id
	@Column(name="PRODUCT_ID", nullable = false, insertable = false, updatable = false)
	private Long product_id;
	
	@Id
	@Column(name="PATIENT_ID", nullable=false, insertable = false, updatable = false)
	private Long patient_id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="PRODUCT_ID",referencedColumnName = "ID")
	@Transient
	private MedicalProduct product;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PATIENT_ID",referencedColumnName = "ID")
	@Transient
	private Patient patient;
	
	private Integer  rating;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}

	public Long getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(Long patient_id) {
		this.patient_id = patient_id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public ProductRating() {
		
	}

	public ProductRating(Long product_id, Long patient_id, Integer rating) {
		this.product_id = product_id;
		this.patient_id = patient_id;
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "ProductRating [product_id=" + product_id + ", patient_id=" + patient_id + ", product=" + product
				+ ", patient=" + patient + ", rating=" + rating + "]";
	}
	
	
	
	
	
}
