package com.daytoday.models;

import java.io.Serializable;  

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;


@Entity
@ApiModel(description = "medical product data")
public class MedicalProduct implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5955951322614767158L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long  id;
	
	private String product_name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
