package com.daytoday.service;

import java.util.List;

import com.daytoday.models.Patient;

public interface PatientService {

	List<Patient> getPatients();
	
	Patient savepatient(Patient p);

}
