package com.daytoday.service;

import java.util.HashMap;  
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daytoday.models.ProRatId;
import com.daytoday.models.ProductRating;
import com.daytoday.models.ProductRatingResult;
import com.daytoday.repository.MedicalProductRepository;
import com.daytoday.repository.ProductRatingRepository;
@Service
public class ProductRatingServiceImpl implements ProductRatingService 
{
	 
	 private ProductRatingRepository productRatingRepo;
	 
	 private MedicalProductRepository medProRepo;
	 
	
	
	@Autowired
	public ProductRatingServiceImpl(ProductRatingRepository productRatingRepo, MedicalProductRepository medProRepo) {
		super();
		this.productRatingRepo = productRatingRepo;
		this.medProRepo = medProRepo;
	}

	
	public List<ProductRating> getRatings()
	{
		return productRatingRepo.findAll();
	}
	
	
	/**
	 *This Method is used to get the average rating of a product,product name,product id
	 *@param Long product id
	 *@return ProductRatingResult type object 
	 */
	public ProductRatingResult getRatingOfProduct(Long p_id)
	{
		if (!medProRepo.existsById(p_id)) return null;
		
		List<Integer> list = productRatingRepo.findRatingByProductid(p_id);
		if (list.isEmpty()) {
			return null;
		}
		ProductRatingResult productRatingResult = new ProductRatingResult();
		if (!list.isEmpty()) 
		{
			productRatingResult.setMedicalProduct(medProRepo.findById(p_id).get());
	
			Double avgRating = list.stream().mapToInt(Integer::intValue).average().orElse(0);
			productRatingResult.setAvgRating(avgRating);
			
			Map<Integer, Integer> ratingCount = new HashMap<>();
			
			for (Integer rating : list)
			{
				Integer countRating=ratingCount.getOrDefault(rating, 0);
				ratingCount.put(rating, countRating+1);
			}
			
			productRatingResult.setRatingCount(ratingCount);
		}
		return productRatingResult;	
	}
	

	public ProductRating saveProductRating(ProductRating productRating)
	{
		 ProRatId proRatId=new ProRatId();
		 proRatId.setPatient_id(productRating.getPatient_id());
		 proRatId.setProduct_id(productRating.getProduct_id());
		 if(productRatingRepo.existsById(proRatId))return null;
		 else return productRatingRepo.save(productRating);
	}
	


	public ProductRating updatePrductRating(ProductRating productRating) {
		 ProRatId proRatId=new ProRatId();
		 proRatId.setPatient_id(productRating.getPatient_id());
		 proRatId.setProduct_id(productRating.getProduct_id());
		 
		 if(!productRatingRepo.existsById(proRatId))return null;
		
		 ProductRating rating = productRatingRepo.findById(proRatId).get();
		 rating.setRating(productRating.getRating());
		 return productRatingRepo.save(rating);	 
	}
}
