package com.daytoday.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daytoday.models.Patient;
import com.daytoday.repository.PatientRepository;
@Service
public class PatientServiceImpl implements PatientService{

	PatientRepository patientRepo;
	
	@Autowired
	public PatientServiceImpl(PatientRepository patientRepo) {
		this.patientRepo = patientRepo;
	}
	
	
	/**
	 *This is used to get list of Patient type object
	 *@return List<Patient>
	 */
	@Override
	public List<Patient> getPatients() {
		return patientRepo.findAll();
	}

	/**
	 * Used to save Patient type object to database
	 *@param Patient
	 *@return Patient
	 */
	@Override
	public Patient savepatient(Patient p) {
		return patientRepo.save(p);
	}


	



}
