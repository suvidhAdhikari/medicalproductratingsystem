package com.daytoday.service;

import java.util.List;

import com.daytoday.models.ProductRating;
import com.daytoday.models.ProductRatingResult;

public interface ProductRatingService {
	
	List<ProductRating> getRatings();
	
	ProductRatingResult getRatingOfProduct(Long p_id);
	
	ProductRating saveProductRating(ProductRating productRating);

	ProductRating updatePrductRating(ProductRating productRating);
}
